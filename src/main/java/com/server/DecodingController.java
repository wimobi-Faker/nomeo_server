package com.server;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.SerializationException;
import com.phoceis.cideo.core.modules.cideo.models.PictureData;
import com.server.testData;


@RestController
public class DecodingController {

    @PostMapping("/decoding")
	public PictureData decoding(@RequestBody testData requestBody) {
        
        String[] response = requestBody.hex.split("(?<=\\G.{2})");
        byte[] finalData = new byte[response.length];

        for (int i = 0; i < response.length; i++) {
            finalData[i] = (byte) (Integer.parseInt(response[i],16) & 0xff);
        }

		try{
			PictureData x = SerializationUtils.deserialize(finalData);
			return x;
		 }catch(SerializationException e){
			 return new PictureData();
		 }
	}
}