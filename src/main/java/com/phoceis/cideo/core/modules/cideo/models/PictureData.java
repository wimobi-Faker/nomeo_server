package com.phoceis.cideo.core.modules.cideo.models;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.phoceis.cideo.app.controllers.models.Template;

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 04/07/2018.
 */
public class PictureData implements Serializable {
    static final long serialVersionUID = -8466445850615758942L;

    @SerializedName("TemplateId")
    public Template template;

    @SerializedName("Field1")
    public String field1;

    @SerializedName("Field2")
    public String field2;

    @SerializedName("Field3")
    public String field3;

    @SerializedName("Field4")
    public String field4;

    @SerializedName("Field5")
    public String field5;

    @SerializedName("Number")
    public String number;

    @SerializedName("Floor")
    public String floor;

    public PictureData() {}

    /**
     * To get picture's datas from device datas
     * @param data - read from device
     * @return readable data from bytes
     */
    public static PictureData fromBytes(byte[] data) {
        String dataString = new String(data);
        return new Gson().fromJson(dataString, PictureData.class);
    }

    private String toJson() {
        return new Gson().toJson(this);
    }

    /**
     * Get data bytes to write into device
     * @return picture datas
     */
    public byte[] toBytes() {
        return toJson().getBytes();
    }

    @Override
    public String toString() {
        return toJson();
    }
}
