package com.phoceis.cideo.app.controllers.models;
import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 04/07/2018.
 */
public class Template implements Serializable {
    static final long serialVersionUID = 4058134730293083193L;

    public final static int TEMPLATE_1_ID = 0;
    public final static int TEMPLATE_2_ID = 1;
    public final static int TEMPLATE_3_ID = 2;
    public final static int TEMPLATE_4_ID = 3;
    public final static int TEMPLATE_5_ID = 4;

    @SerializedName("Id")
    public int id;

    public Template() {}

    public Template(int id) {
        this.id = id;
    }

    public static Template template1() {
        return new Template(TEMPLATE_1_ID);
    }

    public static Template template2() {
        return new Template(TEMPLATE_2_ID);
    }

    public static Template template3() {
        return new Template(TEMPLATE_3_ID);
    }

    public static Template template4() {
        return new Template(TEMPLATE_4_ID);
    }

    public static Template template5() {
        return new Template(TEMPLATE_5_ID);
    }

    public int id() {
        return id;
    }

    // public int background() {
    //     switch (id) {
    //         case TEMPLATE_1_ID:
    //             return R.drawable.template_1_background;
    //         case TEMPLATE_2_ID:
    //             return R.drawable.template_2_background;
    //         case TEMPLATE_3_ID:
    //             return R.drawable.template_3_background;
    //         case TEMPLATE_4_ID:
    //             return R.drawable.template_4_background;
    //         case TEMPLATE_5_ID:
    //             return R.drawable.template_factory;
    //         default:
    //             return 0;
    //     }
    // }
}
